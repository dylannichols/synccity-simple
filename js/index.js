var mySwiper = new Swiper('.swiper-container', {
    speed: 400,
    spaceBetween: 20,
    slidesPerView: 6,
    loop: false,
    watchOverflow: true,
    centerInsufficientSlides: true,
    breakpoints: {
        1200: {
            slidesPerView: 3.5
        },
        823: {
            slidesPerView: 2.5
        },
        480: {
            slidesPerView: 1.5
        }
    }
});

var linkSwiper = new Swiper('.link-container', {
    speed: 400,
    spaceBetween: 20,
    slidesPerView: 5,
    breakpoints: {
        1200: {
            slidesPerView: 3.5
        },
        823: {
            slidesPerView: 2.5
        },
        480: {
            slidesPerView: 1.5
        }
    }
});


$('.swiper-slide').hover(function() {
        if (!$(this).hasClass("boom-slide")) {

            $(this).removeClass('item-next').removeClass('item-prev').addClass('item-active');
            $(this).prevAll().removeClass('item-active').removeClass('item-next').addClass('item-prev');
            $(this).nextAll().removeClass('item-active').removeClass('item-prev').addClass('item-next');
        }
    },
    function() {
        if (!$(this).hasClass("boom-slide")) {
            $('.swiper-slide').removeClass('item-next').removeClass('item-prev').removeClass('item-active');
        }
    })

window.document.onkeydown = function(e) {
    if (!e) {
        e = event;
    }
    if (e.keyCode == 27) {
        lightbox_close();
    }
}

window.mobilecheck = function() {
    var check = false;
    (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};


function lightbox_open(videoSource) {
    getStream(videoSource);

    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    var lightBoxVideo = document.querySelector('video');
    // var elem = document.querySelector("#VisaChipCardVideo");
    // if (elem.requestFullscreen) {
    //     elem.requestFullscreen();
    // } else if (elem.msRequestFullscreen) {
    //     elem.msRequestFullscreen();
    // } else if (elem.webkitRequestFullscreen) {
    //     elem.webkitRequestFullscreen();
    // }
    // lightBoxVideo.play();
}

function lightbox_open2(videoSource) {
    getStream(videoSource);

    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    var lightBoxVideo = document.querySelector('video');
    // var elem = document.querySelector("#VisaChipCardVideo");
    // if (elem.requestFullscreen) {
    //     elem.requestFullscreen();
    // } else if (elem.msRequestFullscreen) {
    //     elem.msRequestFullscreen();
    // } else if (elem.webkitRequestFullscreen) {
    //     elem.webkitRequestFullscreen();
    // }
    // lightBoxVideo.play();
}

function lightbox_close() {
    var lightBoxVideo = document.querySelector('.mistvideo-video');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
}

function getStream(streamname) {
    var c = document.querySelector(".video-container");

    if (c == null) {
        c = document.createElement("div");
        c.classList.add("video-container");
    }
    c.classList.add("video-container");
    c.style.display = "flex";
    c.style.justifyContent = "center";
    c.style.maxHeight = "100vh";
    var container = document.querySelector('#VisaChipCardVideo');
    container.appendChild(c);

    var banner_bottom = document.createElement("img")

    if (streamname === 'remuera01') {
        var banner_srcs = [
            "img/banners/000.png",
            "img/banners/001.png",
            "img/banners/002.png",
            "img/banners/003.png",
            "img/banners/004.png",
            "img/banners/005.png",
            "img/banners/006.png",
            "img/banners/007.png",

        ]
        var i = 0;

        function next_banner() {
            banner_bottom.src = banner_srcs[i];
            i++;
            if (i >= banner_srcs.length) { i = 0; }
        }
        setInterval(function() {
            next_banner();
        }, 7e3);
        next_banner();

    }


    //build the embed player
    mistPlay(streamname, {
        target: c,
        skin: {
            structure: {
                main: {
                    if: function() {
                        return (!!this.info.hasVideo && (this.source.type.split("/")[1] != "audio"));
                    },
                    then: { //use this substructure when there is video
                        type: "placeholder",
                        classes: ["mistvideo"],
                        children: [{
                            type: "hoverWindow",
                            mode: "pos",
                            style: { position: "relative", overflow: "hidden" },
                            transition: {
                                hide: "left: 0; right: 0; bottom: -43px;",
                                show: "bottom: 0;"
                            },
                            button: { type: "video" },
                            children: [{ //banner settings
                                    type: "logo",
                                    element: banner_bottom, //..or a DOM element
                                    style: {
                                        position: "absolute",
                                        width: "100%",
                                        maxHeight: "20%",
                                        objectFit: "contain", //prevents the image from being stretched
                                        bottom: "37px",
                                        left: 0,
                                        right: 0,
                                        filter: "drop-shadow(1px 1px 2px black)",
                                        margin: "0 auto"
                                    }
                                },
                                { type: "loading" },
                                { type: "error" }
                            ],
                            window: { type: "controls" }
                        }]
                    },
                    else: { //use this subsctructure for audio only
                        type: "container",
                        classes: ["mistvideo", "mistvideo-novideo"],
                        children: [{
                            type: "controls",
                            style: { width: "480px" }
                        }, { type: "loading" }, { type: "error" }],
                    }
                }
            }
        }
    });
};




// var overlay = document.getElementById('sponsor_banner');
// var video = document.getElementById('v');
// video.addEventListener('progress', function () {
//     var show = video.currentTime >= 5 && video.currentTime < 10;
//     overlay.style.visibility = show ? 'visible' : 'visible';
// }, false);