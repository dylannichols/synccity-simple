<?php
if ($_POST) {
    $to = 'support@synccity.co.nz';
    $subject = 'Support Request - SyncCity.';
    $message = 'Name: ' . $_POST["name"] . "\nPhone: " . $_POST["phone"] . "\nEmail: " . $_POST["email"] . "\nMessage: " . $_POST["text"];
    $headers = 'From: ' . $_POST["email"] . "\r\n" .
        'Reply-To:'. $_POST["email"] . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
};
?> 